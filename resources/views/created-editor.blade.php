<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <p>Saudara/i <h3>{{ $nama }}</h3>,
        telah membuat blog yang berjudul <h3>{{ $judul }}</h3>,
        Mohon untuk segera direview. Terima Kasih
    </p>
</body>
</html>