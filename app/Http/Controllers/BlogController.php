<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Events\BlogCreated;
use App\Events\BlogPublished;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $blog = Auth::user()->blogs()->create($data);

        event(new BlogCreated($blog));

        return $blog;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        if ($request->has('published')) {
            if (Auth::user()->roles == 'Editor') {
                $blog->update([
                    'is_publish' => request('published')
                ]);

                event(new BlogPublished($blog));

                return $blog;
            } else {
                return 'Youre Not Authorized';
            }
        } else {
            return $request;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        //
    }
}
