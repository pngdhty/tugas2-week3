<?php

namespace App\Listeners;

use App\Blog;
use App\Events\BlogCreated;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\BlogEditorCreatedMail;

class SendEditorNotification implements ShouldQueue
{
    public $blog;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Blog $blog)
    {
        $this->blog = $blog;
    }

    /**
     * Handle the event.
     *
     * @param  BlogCreated  $event
     * @return void
     */
    public function handle(BlogCreated $event)
    {
        $editor = User::where('roles', 'Editor')->get()->toArray();

        Mail::to($editor)->send(new BlogEditorCreatedMail($event->blog));
    }
}
